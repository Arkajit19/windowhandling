package w3schoolwindows;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;

public class W3school {

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","src/test/java/chromedriver_win32/chromedriver.exe");
        WebDriver driver;
        WebElement hg1,sd2,tr3,po5,bh9,jh7;
        ChromeOptions options=new ChromeOptions();
        options.addArguments("--disable-notifications");
        driver=new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("http://www-db.deis.unibo.it/courses/TW/DOCS/w3schools/website/default.asp.html");
        String cord= Keys.chord(Keys.CONTROL,Keys.ENTER);
        String str2="//div[@style='float:left;width:50%;overflow:hidden;height:44px']//following::a[3]";
        hg1= driver.findElement(By.xpath(str2));
        hg1.sendKeys(cord);
        String kj2="//div[@style='float:left;width:50%;overflow:hidden;height:44px']//following::a[4]";
        sd2= driver.findElement(By.xpath(kj2));
        sd2.sendKeys(cord);
        String jn2="//div[@style='float:left;width:50%;overflow:hidden;height:44px']//following::a[5]";
        tr3=driver.findElement(By.xpath(jn2));
        tr3.sendKeys(cord);
        String jn3="//div[@style='float:left;width:50%;overflow:hidden;height:44px']//following::a[6]";
        po5=driver.findElement(By.xpath(jn3));
        po5.sendKeys(cord);
        String jn4="//div[@style='float:left;width:50%;overflow:hidden;height:44px']//following::a[7]";
        bh9=driver.findElement(By.xpath(jn4));
        bh9.sendKeys(cord);
        Thread.sleep(3000);
        ArrayList<String>tbnew1=new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tbnew1.get(4));
        System.out.println("Title of this page : "+driver.getTitle());
        Thread.sleep(3000);
        driver.switchTo().window(tbnew1.get(3));
        System.out.println("Title of this page : "+driver.getTitle());
        Thread.sleep(3000);
        driver.switchTo().window(tbnew1.get(2));
        System.out.println("Title of this page : "+driver.getTitle());
        Thread.sleep(3000);
        driver.switchTo().window(tbnew1.get(1));
        System.out.println("Title of this page : "+driver.getTitle());
        Thread.sleep(3000);
        driver.switchTo().window(tbnew1.get(0));
        System.out.println("Title of this page : "+driver.getTitle());




    }

}
